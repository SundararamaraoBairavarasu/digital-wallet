import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;


public class bfs {

	String[] strarray;
	String streamline;
	int a=0, b=0;
	Queue<Integer> queue;
	Queue<Integer> adjv;
	Set<Integer> visit;
	Map<Integer, Integer>degree1;	
	BufferedReader br;
	BufferedWriter bw1, bw2, bw3;
	graphs gph;
	
	//Constructor initialization
	public bfs(String Batchpath, String Streampath, String out1, String out2, String out3)	
	{
	 gph=new graphs(Batchpath);
	 breadthfirstsearch(Streampath,out1, out2, out3);		
	}
	
	// Breadth First Search is Implemented to find the shortest path between two friends
	public void breadthfirstsearch(String StreamPath, String out1, String out2, String out3)
	{		
	try {
	
	// Buffered stream to read and Write from and to the text files
	br = new BufferedReader(new FileReader(StreamPath));
	File file1 = new File(out1);
	if(!file1.exists()){file1.createNewFile();};
	File file2 = new File(out2);
	if(!file2.exists()){file1.createNewFile();};
	File file3 = new File(out3);
	if(!file3.exists()){file1.createNewFile();};
	
		
	bw1=new BufferedWriter(new FileWriter(file1.getAbsoluteFile()));
	bw2=new BufferedWriter(new FileWriter(file2.getAbsoluteFile()));
	bw3=new BufferedWriter(new FileWriter(file3.getAbsoluteFile()));
	
	
	// BFS is implemented by 1) Enqueue object into the queue, 2) dequeue FIFO queue 3) Iterate all adjacent nodes and Enqueue check whether node is discovered
	
	while ((streamline = br.readLine())!= null)
	{
	strarray=streamline.split(",");
	
	
	// Check for unformatted lines and the column header
	if (strarray.length >= 4) 
	{
	if(strarray[1].trim() != null || strarray[2].trim()!=null)
	{
		if(strarray[1].trim().compareToIgnoreCase("id1") != 0)
		{
	 a=Integer.parseInt(strarray[1].trim());
	 b=Integer.parseInt(strarray[2].trim());
	 
	 visit=new HashSet<Integer>();
	 degree1=new HashMap<Integer,Integer>();
	 queue=new LinkedList<Integer>();
     queue.add((Integer) a);	// Enqueue	
	 visit.add((Integer) a);   // Keep track of nodes visited
	 degree1.put((Integer) a, (Integer) 0); //Array Hashmap to count the degree of separation
 
	while(!queue.isEmpty()){
			Integer nextnode1=queue.peek(); //get the data from the front of the queue
			adjv = gph.getGraph().get(nextnode1);
			queue.poll(); //dequeue
			
			if(adjv!=null){
				
				if((nextnode1==b))
				{
				
					String trusted="trusted";
					String unverified="unverified";
					switch(degree1.get(nextnode1)){
					case 0:
						System.out.println("Self Payment"); // Special case payment made to self for bookkeeping
						bw1.write(trusted+"\n");
						bw2.write(trusted+"\n");
						bw3.write(trusted+"\n");
					    break;										
					case 1:   //1st level Friend
						bw1.write(trusted+"\n");
						bw2.write(trusted+"\n");
						bw3.write(trusted+"\n");
					    break;
					case 2:  //2nd level Friend
						bw1.write(unverified+"\n");
						bw2.write(trusted+"\n");
						bw3.write(trusted+"\n");
					    break;
					case 3: //3rd level Friend
						bw1.write(unverified+"\n");
						bw2.write(unverified+"\n");
						bw3.write(trusted+"\n");
					    break;
					case 4: //4th level Friend
						bw1.write(unverified+"\n");
						bw2.write(unverified+"\n");
						bw3.write(trusted+"\n");
					    break;
					default: // for all other unverified
						bw1.write(unverified+"\n");
						bw2.write(unverified+"\n");
						bw3.write(unverified+"\n");
					    break;						
					}
					break;										
					}
					}					
			
				Iterator<Integer> itadjv=adjv.iterator();				
				while(itadjv.hasNext())
				{
					
					Integer neighbour1=itadjv.next();
					if (!visit.contains(neighbour1)){	
						queue.add(neighbour1);
						visit.add(neighbour1);
						degree1.put(neighbour1, degree1.get(nextnode1)+1);
						
					}
				}
				
				if(queue.isEmpty())  // special case when there is no connecting friend even beyond 4th layer friend network from the graph. First time Venmo transaction need to alert the customer 
				{
					System.out.println("Not in friends network -- Transaction Warning Message - unverified");
					bw1.write("unverified \n");
					bw2.write("unverified \n");
					bw3.write("unverified \n");
					break;	
				}				
			}	
		}
	}

}
	} // close the read write stream
	br.close();
	bw1.close();
	bw2.close();
	bw3.close();
	
	} catch (FileNotFoundException e) {		
		e.printStackTrace();
	} catch (NumberFormatException e) {
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	}catch(java.lang.ArrayIndexOutOfBoundsException e)
	{
		e.printStackTrace();
	}
	
}
	
}	

