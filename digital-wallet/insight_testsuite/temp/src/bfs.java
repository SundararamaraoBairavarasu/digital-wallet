import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;


public class bfs {

	String[] strarray;
	String streamline;
	int a=0, b=0;
	Queue<Integer> queue1;
	Queue<Integer> adj1;
	Set<Integer> visit1;
	Map<Integer, Integer>step1;	
	BufferedReader br;
	BufferedWriter bw1, bw2, bw3;
	graphs gph;
	
	public bfs(String Batchpath, String Streampath, String out1, String out2, String out3)
	
	{
	 gph=new graphs(Batchpath);
	 breadthfirstsearch(Streampath,out1, out2, out3);	
	
	}
	
	public void breadthfirstsearch(String StreamPath, String out1, String out2, String out3)
	{		
	try {
	
	br = new BufferedReader(new FileReader(StreamPath));
	File file1 = new File(out1);
	if(!file1.exists()){file1.createNewFile();};
	File file2 = new File(out2);
	if(!file2.exists()){file1.createNewFile();};
	File file3 = new File(out3);
	if(!file3.exists()){file1.createNewFile();};
	
		
	bw1=new BufferedWriter(new FileWriter(file1.getAbsoluteFile()));
	bw2=new BufferedWriter(new FileWriter(file2.getAbsoluteFile()));
	bw3=new BufferedWriter(new FileWriter(file3.getAbsoluteFile()));
	
	while ((streamline = br.readLine())!= null)
	{
	strarray=streamline.split(",");
	if (strarray.length >= 4) 
	{
	if(strarray[1].trim() != null || strarray[2].trim()!=null)
	{
		if(strarray[1].trim().compareToIgnoreCase("id1") != 0)
		{
	 a=Integer.parseInt(strarray[1].trim());
	 b=Integer.parseInt(strarray[2].trim());
	 
	 visit1=new HashSet<Integer>();
	 step1=new HashMap<Integer,Integer>();
	 queue1=new LinkedList<Integer>();
     queue1.add((Integer) a);		
	visit1.add((Integer) a);
	step1.put((Integer) a, (Integer) 0);
 
	while(!queue1.isEmpty()){
			Integer nextnode1=queue1.peek();
			adj1 = gph.getGraph().get(nextnode1);
			queue1.poll();
			
			if(adj1!=null){
				
				if((nextnode1==b))
				{
				
					String trusted="trusted";
					String unverified="unverified";
					switch(step1.get(nextnode1)){
					case 0:
						System.out.println("Self Payment");
						bw1.write(trusted+"\n");
						bw2.write(trusted+"\n");
						bw3.write(trusted+"\n");
					    break;										
					case 1:
						bw1.write(trusted+"\n");
						bw2.write(trusted+"\n");
						bw3.write(trusted+"\n");
					    break;
					case 2:
						System.out.println("case2");
						bw1.write("unverified\n");
						bw2.write(trusted+"\n");
						bw3.write(trusted+"\n");
					    break;
					case 3:
						bw1.write(unverified+"\n");
						bw2.write(unverified+"\n");
						bw3.write(trusted+"\n");
					    break;
					case 4:
						bw1.write(unverified+"\n");
						bw2.write(unverified+"\n");
						bw3.write(trusted+"\n");
					    break;
					default:
						bw1.write(unverified+"\n");
						bw2.write(unverified+"\n");
						bw3.write(unverified+"\n");
					    break;						
					}
					break;										}
					}					
			
				Iterator<Integer> itadj1=adj1.iterator();				
				while(itadj1.hasNext())
				{
					
					Integer neighbour1=itadj1.next();
					if (!visit1.contains(neighbour1)){
					
						queue1.add(neighbour1);
						visit1.add(neighbour1);
						step1.put(neighbour1, step1.get(nextnode1)+1);
						
					}
				}
				
				if(queue1.isEmpty())
				{
					System.out.println("Not in friends network -- Transaction Warning Message -unverified");
					bw1.write("unverified \n");
					bw2.write("unverified \n");
					bw3.write("unverified \n");
					break;	
				}
				
				
				
			}	
		}
	}

}
	}
	br.close();
	bw1.close();
	bw2.close();
	bw3.close();
	
	} catch (FileNotFoundException e) {		
		e.printStackTrace();
	} catch (NumberFormatException e) {
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	}catch(java.lang.ArrayIndexOutOfBoundsException e)
	{
		e.printStackTrace();
	}
	
}
	
}	

