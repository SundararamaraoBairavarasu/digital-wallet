import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;


public class graphs {
	Map<Integer, LinkedList<Integer>> graph;	
	
	public graphs (String path)
	{
		creategraph(path);	
	}
		
	public void creategraph(String path)
	{
		String [] strs;
		String str;
		int a, b;
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(path));
			graph = new HashMap<Integer, LinkedList<Integer>>();
			
			while ((str = br.readLine())!= null)
			{			
			 strs=str.split(",");
			if(strs.length >= 4) 
			{		
			 if(strs[1].trim() != null || strs[2].trim()!=null)
				{
				 if(strs[1].trim().compareToIgnoreCase("id1") != 0)
					{
		
			 a=Integer.parseInt(strs[1].trim());
			 b=Integer.parseInt(strs[2].trim());
					 
			 if ( graph.containsKey(a)){
				 graph.get(a).add(b);
			 } else {
				 LinkedList<Integer> linkedlist = new LinkedList<Integer>();
				 linkedlist.add(b);
				 graph.put(a, linkedlist);
			 }
			 if(graph.containsKey(b)){
				 graph.get(b).add(a);
				 
			 }else {
				 LinkedList <Integer> linkedlist = new LinkedList<Integer>();
				 linkedlist.add(a);
				 graph.put(b, linkedlist);
			 }				
		  }
		}
	}
}
			 br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch(java.lang.ArrayIndexOutOfBoundsException e)
		{
			e.printStackTrace();	
		}			
	}
	
	public Map<Integer, LinkedList<Integer>> getGraph() {
		return graph;
	}


	public void setGraph(Map<Integer, LinkedList<Integer>> graph) {
		this.graph = graph;
	}


}
